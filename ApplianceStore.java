import java.util.Scanner;

public class ApplianceStore{
	
	public static void main(String[] args) throws InterruptedException{
		
		Scanner input = new Scanner(System.in);
		RiceCooker[] cooker = new RiceCooker[4];
		
		for(int i = 0;i<cooker.length;i++){
			
			cooker[i] = new RiceCooker(input.nextLine(), Integer.parseInt(input.nextLine()),input.nextLine(), Integer.parseInt(input.nextLine()));
			
			System.out.println("Enter the brand of your rice Cooker");
			cooker[i].setBrand(input.nextLine());
			System.out.println("Enter how much rice your rice Cooker can contain");
			cooker[i].setCapacityOfRice(Integer.parseInt(input.nextLine()));
			System.out.println("Enter the name of the tune your rice Cooker plays after it has finish doing its job.");
			cooker[i].setTune(input.nextLine());
			System.out.println("Enter the amount of water you added in the Rice Cooker");
			cooker[i].setWaterQuantity(Integer.parseInt(input.nextLine()));
		}
		System.out.println("The rice capacity is: ");
		cooker[1].setCapacityOfRice(Integer.parseInt(input.nextLine()));
		System.out.println("The tune is: ");
		cooker[1].setTune(input.nextLine());
		System.out.println("The rice qut is: ");
		cooker[1].setWaterQuantity(Integer.parseInt(input.nextLine()));
		
		
		System.out.println("the brand is: " + cooker[cooker.length - 1].getBrand());
		System.out.println("the tune is: " + cooker[cooker.length - 1].getTune());
		System.out.println("the capacity of Rice is: " + cooker[cooker.length - 1].getCapacityOfRice());
		System.out.println("the quantity of water is : " + cooker[cooker.length - 1].getWaterQuantity());
		
		System.out.print("The rice cooker has played: ");
		cooker[1].sing();
		System.out.println("The rice Cooker has cooked : " + cooker[1].riceCooked() + " grams of rice");
		cooker[1].cookRice(cooker[1].getWaterQuantity());
	}
}