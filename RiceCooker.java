public class RiceCooker{
	
	private String brand;
	private int capacityOfRice;
	private String tune;
	private int waterQuantity;
	
	public int getWaterQuantity(){
		
		return this.waterQuantity;
	}
	
	public void setWaterQuantity(int water){
		
		this.waterQuantity = water;
	}
	
	public String getTune(){
		
		return this.tune;
	}
	
	public void setTune(String tune){
		
		this.tune = tune;
	}
	public int getCapacityOfRice(){
		
		return this.capacityOfRice;
	}
	
	public void setCapacityOfRice(int rice){
		
	this.capacityOfRice = rice;
	}
	
	public String getBrand(){
		
		return this.brand;
	}
	// public void setBrand(String brand){
		
	// this.brand = brand;
	// }
	
	public RiceCooker(String brand, int capacityOfRice, String tune, int waterQuantity){
		this.brand = brand;
		this.capacityOfRice = capacityOfRice;
		this.tune = tune;
		this.waterQuantity = waterQuantity;
	}
	
	
	
	public void sing() throws InterruptedException {
		
		int x = 0;
		for(int i = 5; i>0;i--){
			
			Thread.sleep(1000);
			System.out.println(i + "Seconds left");
			
			}
		
		System.out.print(this.tune);
	}
	
	public int riceCooked(){
		return this.capacityOfRice;
	}
	
	public void cookRice(int water) throws InterruptedException{
		if (waterAndRiceQuantityValidator(water)){
			System.out.println("Initial water quantity: " + water);
			this.waterQuantity = water;
			Thread.sleep(2000);
			System.out.println("Rice is cooked, the remaning amount of water is: " + water/20);
		}
		else{
			System.out.println("The entered amount of water is invalid");
		}
	}
	public boolean waterAndRiceQuantityValidator(int amount){
		if (amount > 0)
			return true;
		
		return false;
	}
}	